//! View into a save file and parse data while accessing it.

use std::{error::Error, io::Cursor};

use super::{
    utils::{FileSlotIndex, IngameSlotIndex},
    SaveSlot,
};

pub struct SaveFileView {
    bytes: [u8; Self::BYTE_SIZE],
}

impl SaveFileView {
    const BYTE_SIZE: usize = 512;

    pub fn from_exact_bytes(bytes: [u8; Self::BYTE_SIZE]) -> Self {
        Self { bytes }
    }

    // TODO: Create concrete Errors
    pub fn try_from_byte_slice(bytes: &[u8]) -> Result<Self, Box<dyn Error>> {
        let bytes = bytes.try_into().map_err(|_| "Byte count mismatch")?;

        Ok(Self { bytes })
    }

    pub fn get_view_by_file_index(&self, index: FileSlotIndex) -> &[u8; 120] {
        let offset = SaveSlotView::BYTE_SIZE * (index.as_u8() as usize);
        self.bytes[offset..(offset + SaveSlotView::BYTE_SIZE)]
            .try_into()
            .expect("Offset and size is calculated")
    }

    pub fn get_view_by_ingame_index(&self, index: IngameSlotIndex) -> &[u8; 120] {
        let mut cursor = Cursor::new(&self.bytes[0..(120 * 4)]);

        for i in 0..4 {
            let slot = SaveSlot::parse_from_reader(&mut cursor).unwrap();
            if slot.ingame_index == index {
                let file_slot_index = FileSlotIndex::try_new(i).unwrap();
                return self.get_view_by_file_index(file_slot_index);
            }
        }

        unreachable!(
            "Bytes are always enough for 4 slots, which is the maximum of `IngameSlotIndex`"
        );
    }

    pub fn parse_slots(&self) -> [SaveSlot; 4] {
        let mut cursor = Cursor::new(&self.bytes[0..(120 * 4)]);

        [
            SaveSlot::parse_from_reader(&mut cursor).unwrap(),
            SaveSlot::parse_from_reader(&mut cursor).unwrap(),
            SaveSlot::parse_from_reader(&mut cursor).unwrap(),
            SaveSlot::parse_from_reader(&mut cursor).unwrap(),
        ]
    }
}

pub struct SaveSlotView {
    bytes: [u8; Self::BYTE_SIZE],
}

impl SaveSlotView {
    pub const BYTE_SIZE: usize = 120;

    pub fn from_exact_bytes(bytes: [u8; Self::BYTE_SIZE]) -> Self {
        Self { bytes }
    }

    // TODO: Create concrete Errors
    pub fn try_from_byte_slice(bytes: &[u8]) -> Result<Self, Box<dyn Error>> {
        let bytes = bytes.try_into().map_err(|_| "Byte count mismatch")?;

        Ok(Self { bytes })
    }

    pub fn parse_self(&self) -> Result<SaveSlot, Box<dyn Error>> {
        let mut cursor = Cursor::new(&self.bytes);
        SaveSlot::parse_from_reader(&mut cursor)
    }
}

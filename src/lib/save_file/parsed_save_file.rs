//! Parsed save file into Rust structs

#![allow(unused)]

use std::{
    error::Error,
    io::{Read, Seek},
};

use byteorder::{BigEndian, ReadBytesExt};

use self::utils::IngameSlotIndex;

#[derive(Debug)]
pub struct SaveFile {
    save_slots: [SaveSlot; 4],
    global_data: (),
}

impl SaveFile {
    pub fn new(save_slots: [SaveSlot; 4], global_data: ()) -> Self {
        Self {
            save_slots,
            global_data,
        }
    }

    // TODO: Create concrete Errors
    pub fn parse_from_reader(reader: &mut (impl Read + Seek)) -> Result<Self, Box<dyn Error>> {
        for _ in 0..=3 {
            let slot = SaveSlot::parse_from_reader(reader).unwrap();

            if slot.last_used_unsure_todo {
                dbg!(slot);
            }
        }

        todo!("slots parsed, global data is next");
    }
}

#[derive(Debug, Clone)]
pub struct SaveSlot {
    last_used_unsure_todo: bool,
    pub(crate) ingame_index: IngameSlotIndex,

    // TODO: Create a nice struct for this
    note_high_scores: [u8; 11],

    // TODO: Create a nice struct for this
    ///
    /// [0] => ?
    /// [1] => ?
    /// [2] => ?
    /// [3] => ?
    /// [4] => ?
    /// [5] => GruntysLair
    /// [6] => ?
    /// [7] => ?
    /// [8] => ?
    /// [9] => ?
    /// [10] => ?
    /// [11] => SpiralMountain
    level_times: [u16; 11],

    checksum: u32,
}

impl SaveSlot {
    /// reader has to be in position already
    pub(crate) fn parse_from_reader(reader: &mut (impl Read + Seek)) -> Result<Self, Box<dyn Error>> {
        let last_used_unsure_todo = reader.read_u8()?;
        let ingame_slot_index = IngameSlotIndex::try_new(reader.read_u8()?)?;

        let pos = reader.seek(std::io::SeekFrom::Current(40))?;
        // dbg!(pos);
        // Should be at level-times now

        let mut level_times = [0u16; 11];
        for i in 0..11 {
            let current_time = reader.read_u16::<BigEndian>()?;
            level_times[i] = current_time;
        }
        // dbg!(level_times);

        let pos = reader.seek(std::io::SeekFrom::Current(0))?;
        dbg!(pos);
        // Should be at 64 now

        let pos = reader.seek(std::io::SeekFrom::Current(52))?;
        dbg!(pos);
        // Should be at 116 now

        let checksum = reader.read_u32::<BigEndian>()?;
        dbg!(checksum);

        Ok(Self {
            last_used_unsure_todo: last_used_unsure_todo == 0x11,
            ingame_index: ingame_slot_index,
            note_high_scores: [0; 11],
            level_times,
            checksum,
        })
    }
}

#[derive(Debug)]
pub enum World {
    SpiralMountain,
    GruntysLair,
    MumbosMountain,
    TreasureTroveCove,
    ClankersCavern,
    BubblegloobSwamp,
    FreezeezyPeak,
    GobisValley,
    MadMonsterMansion,
    RustyBucketBay,
    ClickClockWood,
}

pub struct SaveSlots([SaveSlot; 4]);

impl SaveSlots {
    pub fn get_by_ingame_index(&self, index: IngameSlotIndex) -> &SaveSlot {
        self.0
            .iter()
            .find(|s| s.ingame_index == index)
            .expect("IngameSlotIndex cannot be invalid")
    }
}

// Util Structs
pub mod utils {
    use std::error::Error;

    /// Used to index by ingame order
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
    pub struct IngameSlotIndex(u8);

    impl IngameSlotIndex {
        // TODO: Create concrete Error
        pub fn try_new(index: u8) -> Result<Self, Box<dyn Error>> {
            if index > 3 {
                return Err(format!("Index too large ({index})").into());
            }

            Ok(Self(index))
        }

        pub fn as_u8(&self) -> u8 {
            self.0
        }
    }

    /// Used to index by file order
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
    pub struct FileSlotIndex(u8);

    impl FileSlotIndex {
        // TODO: Create concrete Error
        pub fn try_new(index: u8) -> Result<Self, Box<dyn Error>> {
            if index > 3 {
                return Err(format!("Index too large ({index})").into());
            }

            Ok(Self(index))
        }

        pub fn as_u8(&self) -> u8 {
            self.0
        }
    }
}

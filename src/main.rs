use std::{fs, io::Cursor, path::{Path, PathBuf}};

use bkedit::save_file::{utils::IngameSlotIndex, SaveFile, SaveFileView};

fn main() {
    let file_path = file();
    let content = fs::read(&file_path).unwrap();
    let mut cursor = Cursor::new(content.clone());

    let save_file_view = SaveFileView::try_from_byte_slice(&content.clone()).unwrap();
    dump_ingame_slot(save_file_view, 1, &file_path);

    let save_file = SaveFile::parse_from_reader(&mut cursor).unwrap();
    dbg!(save_file);
}

fn dump_ingame_slot(view: SaveFileView, index: u8, original_file: &Path) {
    let slot_index = IngameSlotIndex::try_new(1).unwrap();
    let bytes = view.get_view_by_ingame_index(slot_index);

    let stem = original_file.file_stem().unwrap().to_str().unwrap();
    let extension = original_file.extension().unwrap().to_str().unwrap();
    let path = format!("./data/dump/{stem}.{index}.{extension}");
    fs::write(&path, bytes).expect("Failed to write dump");

    println!("Dumped ingame slot {index} to {path}");
}

fn file() -> PathBuf {
    PathBuf::from("data/Banjo-Kazooie Savegames/snapshot-1 - Clear.eep")
}
